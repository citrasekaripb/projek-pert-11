import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://twitter.com/')

WebUI.click(findTestObject('Page_Twitter Its whats happening  Twitter/div_Sign up'))

WebUI.setText(findTestObject('Page_Sign up for Twitter  Twitter/input_Name_name'), 'susi')

WebUI.setText(findTestObject('Page_Sign up for Twitter  Twitter/input_Phone_phone_number'), '081294804338')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Sign up for Twitter  Twitter/select_month'), '6', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Sign up for Twitter  Twitter/select_date'), '15', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Sign up for Twitter  Twitter/select_year'), '1998', true)

WebUI.click(findTestObject('Object Repository/Page_Sign up for Twitter  Twitter/span_Next'))

WebUI.click(findTestObject('Page_Twitter Its whats happening  Twitter/span_Next'))

